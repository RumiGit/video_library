import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: string[], searchQuery: string): string[] {
    if (!searchQuery) {
      return items;
    }
    return items.filter(item => item.toLowerCase().includes(searchQuery.toLowerCase()));
  }

}
